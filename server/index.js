import cors from "cors";
import express from "express";
import { createServer } from "http";
import { accessToken } from "./controllers/index.js";
import Router from "./routes/index.routes.js";
import UserRouter from "./routes/user.routes.js";
import sequelize from "./utils/database.js";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
const httpServer = createServer(app);

const PORT = 4000;

httpServer.listen(PORT, () => {
  console.log(`Listening on ${PORT} 🚀`);
});

app.get("/auth", accessToken, (req, res) => {
  res.send(req.user);
});
app.use("/users", UserRouter);
app.use("/index", Router);

const connectDb = async () => {
  try {
    await sequelize.authenticate();
    await sequelize.sync({ alter: true });
    console.log("Login has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
};

connectDb();
