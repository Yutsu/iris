import nodemailer from "nodemailer";

export const sendMail = (req, res) => {
  const { template } = req.body;

  const transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com",
    secure: false,
    port: 587,
    tls: {
      ciphers: "SSLv3",
    },
    auth: {
      user: "iris.ecv@outlook.com",
      pass: "&!ECV.IRIS20",
    },
  });

  const mailOptions = {
    from: '"Iris " <iris.ecv@outlook.com>',
    to: "eliott.raheriarisoa@gmail.com",
    subject: "Demande de permis",
    html: template,
  };

  transporter.sendMail(mailOptions, (err, info) => {
    if (err) {
      console.log("err", err);
      return res
        .status(500)
        .send({ message: "Un problème est survenu lors de l'envoi du mail" });
    } else {
      console.log("E-mail envoyé", info.response);
      return res.status(200).send({ message: "Le mail a bien été envoyé !" });
    }
  });
};
