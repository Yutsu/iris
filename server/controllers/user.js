import bcrypt from "bcrypt"
import jwt from "jsonwebtoken"
import { Op } from "sequelize"
import { User } from "../models/index.js"

export const addUser = async (req, res) => {
  const { firstname, lastname, birthdate, username, email, password } = req.body

  const salt = await bcrypt.genSaltSync(10, "a")
  const newPassword = bcrypt.hashSync(password, salt)

  try {
    await User.create({
      firstname: firstname,
      lastname: lastname,
      birthdate: birthdate,
      username: username,
      email: email,
      password: newPassword,
    })
    res.sendStatus(201)
  } catch (err) {
    console.log("error", err)
    res.sendStatus(500)
  }
}

export const deleteUser = async (req, res) => {
  const { username } = req.body

  try {
    await User.destroy({
      where: {
        username: username,
      },
    })
    return res.status(200).send({ username: username })
  } catch (err) {
    return res.status(500).send({ message: "Error when save to database" })
  }
}

export const loginUser = async (req, res) => {
  const { userinfo, password } = req.body

  try {
    const user = await User.findOne({
      where: {
        [Op.or]: [{ username: userinfo }, { email: userinfo }],
      },
    })

    const login = await bcrypt.compareSync(password, user.password)

    if (!login) {
      return res.status(401).send({ message: "Password does not match" })
    }

    const accessToken = jwt.sign(
      { username: user.username, email: user.email },
      "]<b6z?9>Il|$=ntOe#5VA7cudY2U"
    )

    return res.status(200).json({ accessToken })
  } catch (e) {
    return res
      .status(404)
      .send({ message: "No username or email address matches" })
  }
}

export const getOwnUser = async (req, res) => {
  const headers = req.headers["authorization"]
  const token = headers && headers.split(" ")[1]

  const userToken = jwt.decode(token, "]<b6z?9>Il|$=ntOe#5VA7cudY2U")

  const user = await User.findOne({
    where: {
      [Op.or]: [{ username: userToken.username }, { email: userToken.email }],
    },
    attributes: ["username", "email", "firstname", "lastname", "birthdate"],
  })

  return res.status(200).send({ user })
}

export const getUser = async (req, res) => {
  const { username } = req.body

  try {
    const user = await User.findOne({
      where: {
        [Op.or]: [{ username }],
      },
      attributes: ["username", "firstname", "lastname", "birthdate"],
    })

    return res.status(200).send({ user })
  } catch (err) {
    console.log(err)

    return res.sendStatus(404)
  }
}
