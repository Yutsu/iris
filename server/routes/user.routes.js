import express from "express";
import { sendMail } from "../controllers/mail.js";
import {
  addUser,
  deleteUser,
  getOwnUser,
  getUser,
  loginUser,
} from "../controllers/user.js";

const UserRouter = express.Router();

UserRouter.post("/add", addUser);

UserRouter.get("/getOwnUser", getOwnUser);

UserRouter.post("/getUser", getUser);

UserRouter.post("/connect", loginUser);

UserRouter.post("/delete", deleteUser);

UserRouter.post("/sendMail", sendMail);

export default UserRouter;
