import { DataTypes } from "sequelize";
import sequelize from "../utils/database.js";

const UserRole = sequelize.define(
  "UserRole",
  {},
  {
    // Other model options go here
  }
);

export default UserRole;
