import { DataTypes } from "sequelize";
import sequelize from "../utils/database.js";

const Role = sequelize.define(
  "Role",
  {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    // Other model options go here
  }
);

export default Role;
