import User from "./User.js";
import UserRole from "./UserRole.js";
import Role from "./Role.js";

User.hasOne(UserRole, { onDelete: "CASCADE" });
UserRole.belongsTo(User);
UserRole.hasOne(Role, { onDelete: "CASCADE" });
Role.belongsTo(UserRole);

export { User, UserRole, Role };
