const user = "users/";
const index = "index/";
const auth = "auth/";

const BackEndPoints = {
  addUser: user + "add",
  connectUser: user + "connect",
  deleteUser: user + "delete",
  getOwnUser: user + "getOwnUser",
  getUser: user + "getUser",
  sendMail: user + "sendMail",
  generateJwt: index + "generate",
  decodeJwt: index + "decode",
  auth: auth,
};

export default BackEndPoints;
