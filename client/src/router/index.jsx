import { Route, Routes } from "react-router-dom"
import { Associations } from "../pages/Associations"
import { Favorites } from "../pages/Favorites"
import { Form } from "../pages/Form"
import { Home } from "../pages/Home"
import { InfoVille } from "../pages/InfoVille"
import { Login } from "../pages/Login"
import { NotFound } from "../pages/NotFound"
import { Profile } from "../pages/Profile"
import { Signup } from "../pages/Signup"
import FrontEndPoints from "./FrontEndPoints"
import { ProtectedRoute } from "./ProtectedRoute"

export const Router = () => {
  return (
    <Routes>
      <Route
        path={FrontEndPoints.home}
        element={
          <ProtectedRoute>
            <Home />
          </ProtectedRoute>
        }
      />
      <Route path={FrontEndPoints.signup} element={<Signup />} />
      <Route path={FrontEndPoints.login} element={<Login />} />
      <Route
        path={FrontEndPoints.infoville}
        element={
          <ProtectedRoute>
            <InfoVille />
          </ProtectedRoute>
        }
      />
      <Route
        path={FrontEndPoints.associations}
        element={
          <ProtectedRoute>
            <Associations />
          </ProtectedRoute>
        }
      />
      <Route
        path={FrontEndPoints.login}
        element={
          <ProtectedRoute>
            <Login />
          </ProtectedRoute>
        }
      />
      <Route
        path={FrontEndPoints.favorites}
        element={
          <ProtectedRoute>
            <Favorites />
          </ProtectedRoute>
        }
      />
      <Route
        path={FrontEndPoints.user}
        element={
          <ProtectedRoute>
            <Profile />
          </ProtectedRoute>
        }
      />
      <Route
        path={FrontEndPoints.form}
        element={
          <ProtectedRoute>
            <Form />
          </ProtectedRoute>
        }
      />
      <Route path="*" exact={true} element={<NotFound />} />
    </Routes>
  )
}
