import axios from "axios"
import { useCallback, useEffect } from "react"
import { useNavigate } from "react-router-dom"
import BackEndPoints from "../BackEndPoints"
import FrontEndPoints from "../FrontEndPoints"

export const ProtectedRoute = ({ children }) => {
  const navigate = useNavigate()

  const protectRoute = useCallback(async () => {
    try {
      await axios.get(BackEndPoints.auth)
    } catch (_) {
      navigate(FrontEndPoints.login)
    }
  }, [navigate])

  useEffect(() => {
    protectRoute()
  }, [protectRoute])

  return children
}
