const FrontEndPoints = {
  home: "/",
  signup: "/signup",
  login: "/login",
  account: "/account",
  infoville: "/infoville",
  associations: "/associations",
  // gameId: "game/:id",
  favorites: "/favorites",
  form: "/form",
  user: "user/:username",
  // gameId: "game/:id",
}

export default FrontEndPoints
