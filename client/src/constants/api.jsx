export const API_URL_PERMIS_VEGETALISER =
  "https://opendata.paris.fr/api/records/1.0/search/?dataset=permis-de-vegetaliser&q=&lang=fr&rows=100&start=0"

export const API_URL_ESPACES_VERTS =
  "https://opendata.paris.fr/api/records/1.0/search/?dataset=espaces_verts&q=&rows=726&facet=type_ev&facet=categorie&facet=adresse_codepostal&facet=presence_cloture&facet=ouvert_ferme&refine.type_ev=D%C3%A9corations+sur+la+voie+publique"
