import { createTheme } from "@mui/material";

export const theme = createTheme({
  palette: {
    primary: {
      main: "#1A1B28",
    },
    secondary: {
      main: "#6F895D",
    },
    info: {
      main: "#000",
    },
    neutral: {
      main: "#64748B",
    },
    error: {
      main: "#e53e3e",
    },
  },
});
