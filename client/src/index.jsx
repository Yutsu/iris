import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import axios from "axios";
import Cookies from "js-cookie";
import { ThemeProvider } from "@mui/material";
import { theme } from "./utils/GlobalStyles";

const token = Cookies.get("accessToken");

axios.defaults.baseURL = process.env.REACT_APP_URL;

if (token) {
  axios.defaults.headers.common["authorization"] = `Bearer ${token}`;
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <ThemeProvider theme={theme}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </ThemeProvider>
);
