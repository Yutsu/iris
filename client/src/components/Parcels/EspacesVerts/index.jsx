import FavoriteBorderRoundedIcon from "@mui/icons-material/FavoriteBorderRounded"
import FavoriteRoundedIcon from "@mui/icons-material/FavoriteRounded"
import GrassIcon from "@mui/icons-material/Grass"
import LocalFloristIcon from "@mui/icons-material/LocalFlorist"
import YardRoundedIcon from "@mui/icons-material/YardRounded"
import { Box, Button } from "@mui/material"
import Drawer from "@mui/material/Drawer"
import { Fragment, useEffect, useState } from "react"
import "./index.css"

export const EspacesVerts = ({
  dataParcel,
  closeDrawer,
  selectedId,
  open,
  keyList,
}) => {
  const renderPerimeter = () => {
    if (dataParcel.fields.perimeter) {
      return `Périmètre : ${dataParcel.fields.perimeter.toFixed(2)}m²`
    }
    return ""
  }
  const renderSurface = () => {
    if (dataParcel.fields.surface_totale_reelle) {
      return `Surface totale réelle : ${dataParcel.fields.surface_totale_reelle}m²`
    }
    return ""
  }
  const renderCloture = () => {
    if (dataParcel.fields.presence_cloture === "Oui") {
      return ` - Présence d'une clôture`
    } else {
      return ` - Sans clôture`
    }
  }
  const renderIconDecoMail = () => {
    if (
      dataParcel.fields.categorie === "Decoration" ||
      dataParcel.fields.categorie === "Mail"
    ) {
      return <LocalFloristIcon />
    }
  }

  const renderIconJardin = () => {
    if (
      dataParcel.fields.categorie === "Jardiniere" ||
      dataParcel.fields.categorie === "Jardin" ||
      dataParcel.fields.categorie === "Jardinet"
    ) {
      return <YardRoundedIcon />
    }
  }

  const renderIconPelouse = () => {
    if (
      dataParcel.fields.categorie === "Pelouse" ||
      dataParcel.fields.categorie === "Plate-bande" ||
      dataParcel.fields.categorie === "Talus" ||
      dataParcel.fields.categorie === "Terre-plein" ||
      dataParcel.fields.categorie === "Square"
    ) {
      return <GrassIcon />
    }
  }

  const [isFavorite, setIsFavorite] = useState(
    localStorage.getItem(`fav_${dataParcel.recordid}`) === "true"
  )

  const handleFavoriteClick = () => {
    setIsFavorite((prevIsFavorite) => {
      const newIsFavorite = !prevIsFavorite
      localStorage.setItem(`fav_${dataParcel.recordid}`, newIsFavorite)
      return newIsFavorite
    })
  }

  const capitalizeFirstLetter = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }

  useEffect(() => {
    const storedIsFavorite =
      localStorage.getItem(`fav_${dataParcel.recordid}`) === "true"
    setIsFavorite(storedIsFavorite)
  }, [dataParcel.recordid])

  function demand() {
    window.location.href =
      "https://v70-auth.paris.fr/auth/realms/paris/protocol/openid-connect/auth?client_id=G07-PROD&response_type=code&redirect_uri=https%3A%2F%2Fteleservices.paris.fr%2Fpermisdevegetaliser%2Fservlet%2Fplugins%2Foauth2%2Fcallback&scope=openid&state=e0bf4f1f2138&nonce=347bc8092a27f"
  }

  return (
    <Fragment key={"bottom"}>
      <Drawer
        key={keyList}
        anchor={"bottom"}
        open={dataParcel.recordid === selectedId && open}
        onClose={closeDrawer}
        // onKeyDown={closeDrawer}
        // borderRadius={"21px 21px 0 0"}
      >
        <Box key={keyList} className="card" id={dataParcel.recordid}>
          <div className="header_card">
            <h1>
              {capitalizeFirstLetter(dataParcel.fields.nom_ev.toLowerCase())}
            </h1>
            {isFavorite ? (
              <FavoriteRoundedIcon
                className="favorite"
                style={{ cursor: "pointer" }}
                onClick={handleFavoriteClick}
              />
            ) : (
              <FavoriteBorderRoundedIcon
                className="favorite_border"
                style={{ cursor: "pointer" }}
                onClick={handleFavoriteClick}
              />
            )}
          </div>
          <p className="adress">
            {dataParcel.fields.adresse_numero}{" "}
            {dataParcel.fields.adresse_typevoie.toLowerCase()}{" "}
            {dataParcel.fields.adresse_libellevoie.toLowerCase()},{" "}
            {dataParcel.fields.adresse_codepostal}
          </p>
          <p className="type">
            {renderIconDecoMail()}
            {renderIconJardin()}
            {renderIconPelouse()}
            {dataParcel.fields.categorie}
            {renderCloture()}
          </p>
          <p className="status green_space">
            <span></span>Espaces verts publics
          </p>
          <Box sx={{ m: 2 }} />
          <p>Type : {dataParcel.fields.type_ev}</p>
          <p>{renderSurface()}</p>
          <p>Présence de culture : {dataParcel.fields.presence_cloture}</p>
          <p>{renderPerimeter()}</p>
          <Button className="button-demand" type="button" onClick={demand}>
            Faire une demande de permis
          </Button>
        </Box>
      </Drawer>
    </Fragment>
  )
}
