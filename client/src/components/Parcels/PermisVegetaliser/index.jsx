import FavoriteBorderRoundedIcon from "@mui/icons-material/FavoriteBorderRounded"
import FavoriteRoundedIcon from "@mui/icons-material/FavoriteRounded"
import FenceRoundedIcon from "@mui/icons-material/FenceRounded"
import NatureRoundedIcon from "@mui/icons-material/NatureRounded"
import NotInterestedRoundedIcon from "@mui/icons-material/NotInterestedRounded"
import PanoramaFishEyeRoundedIcon from "@mui/icons-material/PanoramaFishEyeRounded"
import SignpostRoundedIcon from "@mui/icons-material/SignpostRounded"
import YardRoundedIcon from "@mui/icons-material/YardRounded"
import { Box, Button } from "@mui/material"
import Drawer from "@mui/material/Drawer"
import { Fragment, useEffect, useState } from "react"
import "./index.css"

export const PermisVegetaliser = ({
  dataParcel,
  closeDrawer,
  selectedId,
  open,
  keyList,
}) => {
  const renderFieldType = () => {
    switch (dataParcel.fields.rv_type) {
      case "Pieds d’arbres":
        return <NatureRoundedIcon />
      case "Jardinières mobiles ou keyholes":
        return <YardRoundedIcon />
      case "Fosses":
        return <PanoramaFishEyeRoundedIcon />
      case "Murs ou clôtures":
        return <FenceRoundedIcon />
      case "Potelets (poteaux anti-stationnement)":
        return <SignpostRoundedIcon />
      case "Barrières fixes anti-stationnement":
        return <NotInterestedRoundedIcon />
      default:
        return <NotInterestedRoundedIcon />
    }
  }

  const [isFavorite, setIsFavorite] = useState(
    localStorage.getItem(`fav_${dataParcel.recordid}`) === "true"
  )

  const handleFavoriteClick = () => {
    setIsFavorite((prevIsFavorite) => {
      const newIsFavorite = !prevIsFavorite
      localStorage.setItem(`fav_${dataParcel.recordid}`, newIsFavorite)
      return newIsFavorite
    })
  }

  useEffect(() => {
    const storedIsFavorite =
      localStorage.getItem(`fav_${dataParcel.recordid}`) === "true"
    setIsFavorite(storedIsFavorite)
  }, [dataParcel.recordid])

  function demand() {
    window.open(
      "https://v70-auth.paris.fr/auth/realms/paris/protocol/openid-connect/auth?client_id=G07-PROD&response_type=code&redirect_uri=https%3A%2F%2Fteleservices.paris.fr%2Fpermisdevegetaliser%2Fservlet%2Fplugins%2Foauth2%2Fcallback&scope=openid&state=e0bf4f1f2138&nonce=347bc8092a27f",
      "_blank"
    )
  }

  const listItem = (anchor) => (
    <Box
      key={keyList}
      className="card"
      sx={{ width: anchor === "bottom" ? "auto" : 250 }}
      id={dataParcel.recordid}
    >
      <div className="header_card">
        <h1>{dataParcel.fields.rv_l_deb}</h1>
        {isFavorite ? (
          <FavoriteRoundedIcon
            className="favorite"
            style={{ cursor: "pointer" }}
            onClick={handleFavoriteClick}
          />
        ) : (
          <FavoriteBorderRoundedIcon
            className="favorite_border"
            style={{ cursor: "pointer" }}
            onClick={handleFavoriteClick}
          />
        )}
      </div>
      <p className="type">
        {renderFieldType()}
        {dataParcel.fields.rv_type}
      </p>
      {dataParcel.fields.statut === "13 - Permis délivré" && (
        <p className="status delivred">
          <span></span>Permis délivré
        </p>
      )}

      <Button className="button-demand" type="button" onClick={demand}>
        Faire une demande de permis*
      </Button>
      <p className="note">
        * Un permis est déjà en cours ici, mais vous pouvez faire une demande
        pour prendre le relais.{" "}
      </p>
    </Box>
  )

  return (
    <div>
      <Fragment key={"bottom"}>
        <Drawer
          key={keyList}
          anchor={"bottom"}
          open={dataParcel.recordid === selectedId && open}
          onClose={closeDrawer}
        >
          {listItem("bottom")}
        </Drawer>
      </Fragment>
    </div>
  )
}
