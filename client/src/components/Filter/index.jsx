import CloseIcon from "@mui/icons-material/Close"
import TuneIcon from "@mui/icons-material/Tune"
import { Box, Button, Divider, Drawer, IconButton } from "@mui/material"
import { Fragment, useState } from "react"
import arbre from "../../assets/arbre.png"
import jardiniere from "../../assets/jardiniere.png"
import { ButtonFilter } from "./Button"
import { Button2Filter } from "./Button2"
import "./index.css"

export const Filter = ({ onChangeFilter }) => {
  const [state, setState] = useState(false)
  const [filterPermis, setFilterPermis] = useState("permis")
  const [filterEspace, setFilterEspace] = useState()

  const toggleDrawer = (_, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Shift" || event.key === "Tab")
    ) {
      return
    }
    setState(open)
  }

  const onclickArbreJardiniere = (value) => {
    setFilterEspace(value)
  }

  const onClickSubmitFilter = () => {
    onChangeFilter(filterPermis, filterEspace)
  }

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      className="boxStyle"
    >
      <CloseIcon className="close" onClick={toggleDrawer(false)} />
      <h1 className="layout">Filtres</h1>
      <Divider />
      <ButtonFilter
        valueFilter="permis"
        filtered={filterPermis}
        setFiltered={setFilterPermis}
        style={{ paddingTop: 8 }}
      />
      <ButtonFilter
        valueFilter="espace"
        filtered={filterPermis}
        setFiltered={setFilterPermis}
        style={{ paddingBottom: 8 }}
      />
      {filterPermis === "permis" && (
        <>
          <Divider />
          <div className="buttonContainer" style={{ paddingTop: 8 }}>
            <div
              className="button"
              onClick={() => onclickArbreJardiniere("")}
              style={{
                padding: 8,
                border: !filterEspace ? "1px solid #1A1B28" : undefined,
              }}
            >
              <p
                style={{
                  fontWeight: !filterEspace ? "bold" : undefined,
                }}
              >
                Tous
              </p>
            </div>
          </div>
          <Button2Filter
            valueFilter="Jardinières mobiles ou keyholes"
            filteredArbre={filterEspace}
            setFilteredArbre={setFilterEspace}
            icon={jardiniere}
          />
          <Button2Filter
            valueFilter="Pieds d’arbres"
            filteredArbre={filterEspace}
            setFilteredArbre={setFilterEspace}
            icon={arbre}
          />
        </>
      )}
      <div className="buttonContainer" onClick={onClickSubmitFilter}>
        <Button
          size="medium"
          variant="contained"
          className="buttonFilter"
          onClick={toggleDrawer(false)}
        >
          Appliquer les filtres
        </Button>
      </div>
    </Box>
  )

  return (
    <Fragment key={"bottom"}>
      <div className="filter">
        <IconButton
          onClick={toggleDrawer("bottom", true)}
          className="icon"
          component="label"
        >
          <TuneIcon className="filterIcon" />
        </IconButton>
        <Drawer
          anchor={"bottom"}
          open={state}
          onClose={toggleDrawer("bottom", false)}
          className="drawer"
        >
          {list("bottom")}
        </Drawer>
      </div>
    </Fragment>
  )
}
