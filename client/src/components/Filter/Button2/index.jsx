export const Button2Filter = ({
  valueFilter,
  filteredArbre,
  setFilteredArbre,
  icon,
}) => {
  const onclickArbreJardiniere = (value) => {
    if (valueFilter === "Tous") {
      setFilteredArbre("")
    }
    setFilteredArbre(value)
  }

  const renderText = () => {
    if (valueFilter === "Jardinières mobiles ou keyholes") {
      return "Jardinière"
    }
    return valueFilter
  }

  return (
    <div className="buttonContainer">
      <div
        className="button"
        onClick={() => onclickArbreJardiniere(valueFilter)}
        style={{
          padding: 8,
          border:
            filteredArbre === valueFilter ? "1px solid #1A1B28" : undefined,
        }}
      >
        {icon && <img src={icon} alt={valueFilter} width="16px" />}
        <p
          style={{
            fontWeight: filteredArbre === valueFilter ? "bold" : undefined,
          }}
        >
          {renderText()}
        </p>
      </div>
    </div>
  )
}
