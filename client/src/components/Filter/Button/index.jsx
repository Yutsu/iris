export const ButtonFilter = ({ valueFilter, filtered, setFiltered, style }) => {
  const onclickPermisEspace = (value) => {
    setFiltered(value)
  }

  const renderValueFilter = () => {
    if (valueFilter === "permis") {
      return {
        color: "dot orange",
        text: "Parcelles avec permis",
      }
    } else
      return {
        color: "dot purple",
        text: "Espaces verts publics",
      }
  }

  return (
    <div className="buttonContainer" style={style}>
      <div
        className="button"
        onClick={() => onclickPermisEspace(valueFilter)}
        style={{
          padding: 8,
          border: filtered === valueFilter ? "1px solid #1A1B28" : undefined,
        }}
      >
        <span className={renderValueFilter().color} />
        <p
          style={{ fontWeight: filtered === valueFilter ? "bold" : undefined }}
        >
          {renderValueFilter().text}
        </p>
      </div>
    </div>
  )
}
