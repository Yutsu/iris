import CloseRoundedIcon from "@mui/icons-material/CloseRounded"
import FavoriteBorderRoundedIcon from "@mui/icons-material/FavoriteBorderRounded"
import GavelRoundedIcon from "@mui/icons-material/GavelRounded"
import MapIcon from "@mui/icons-material/Map"
import MenuRoundedIcon from "@mui/icons-material/MenuRounded"
import PersonOutlineRoundedIcon from "@mui/icons-material/PersonOutlineRounded"
import VolunteerActivismIcon from "@mui/icons-material/VolunteerActivism"
import axios from "axios"
import Cookies from "js-cookie"
import { useCallback, useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import BackEndPoints from "../../router/BackEndPoints"
import FrontEndPoints from "../../router/FrontEndPoints"
import "./Nav.css"

function Nav() {
  const navigate = useNavigate()
  const [user, setUser] = useState({})

  const [navbarOpen, setNavbarOpen] = useState(false)

  const handleToggle = () => {
    setNavbarOpen(!navbarOpen)
  }

  const closeMenu = () => {
    setNavbarOpen(false)
  }

  const disconnect = () => {
    Cookies.remove("accessToken")
    navigate(FrontEndPoints.login)
  }

  const getUser = useCallback(async () => {
    const resp = await axios.get(BackEndPoints.getOwnUser)
    setUser(resp.data.user)
  }, [])

  useEffect(() => {
    getUser()
  }, [getUser])

  return (
    <nav className="navbar">
      {navbarOpen ? (
        <button className="nabvar_burger burger_close" onClick={handleToggle}>
          <CloseRoundedIcon
            style={{
              color: "#fff",
              width: "24px",
              height: "24px",
            }}
          />
        </button>
      ) : (
        <button className="nabvar_burger burger_open" onClick={handleToggle}>
          <MenuRoundedIcon
            style={{ color: "#1A1B28", width: "30px", height: "30px" }}
          />
        </button>
      )}

      <div className={`navbar_menu ${navbarOpen ? " showMenu" : ""}`}>
        <div className="navbar_account">
          <img
            src="https://fraisesdefrance.fr/wp-content/uploads/2021/02/ronde-big.png"
            width="70px"
            alt="user_profile"
          />
          <p>{user.username}</p>
        </div>

        <ul>
          <li className="navbar_item slideInDown-1">
            <PersonOutlineRoundedIcon className="icon_navbar" />
            <a
              href={"user/" + user.username}
              className="navbar_link"
              onClick={() => closeMenu()}
            >
              Profil
            </a>
          </li>
          <li className="navbar_item slideInDown-2">
            <MapIcon className="icon_navbar" />
            <a
              href={FrontEndPoints.home}
              className="navbar_link"
              onClick={() => closeMenu()}
            >
              Carte
            </a>
          </li>
          <li className="navbar_item slideInDown-3">
            <GavelRoundedIcon className="icon_navbar" />
            <a
              href={FrontEndPoints.form}
              className="navbar_link"
              onClick={() => closeMenu()}
            >
              Demande de permis
            </a>
          </li>
          <li className="navbar_item slideInDown-4">
            <FavoriteBorderRoundedIcon className="icon_navbar" />
            <a
              href={FrontEndPoints.favorites}
              className="navbar_link"
              onClick={() => closeMenu()}
            >
              Favoris
            </a>
          </li>
          <li className="navbar_item slideInDown-5">
            <VolunteerActivismIcon className="icon_navbar" />
            <a
              href="/associations"
              className="navbar_link"
              onClick={() => closeMenu()}
            >
              Associations
            </a>
          </li>
        </ul>

        <div className="navbar_logout" onClick={disconnect}>
          <li className="navbar_item slideInDown-5 logout">Se déconnecter</li>
        </div>
      </div>
    </nav>
  )
}

export default Nav
