import L from "leaflet"
import { useEffect, useState } from "react"
import { MapContainer, Marker, Polygon, TileLayer } from "react-leaflet"
import marker1 from "../../assets/marker1.svg"
import marker2 from "../../assets/marker2.svg"
import {
  API_URL_ESPACES_VERTS,
  API_URL_PERMIS_VEGETALISER,
} from "../../constants/api"
import { Filter } from "../Filter"
import Nav from "../Navbar/Nav"
import { EspacesVerts } from "../Parcels/EspacesVerts"
import { PermisVegetaliser } from "../Parcels/PermisVegetaliser"
import "./index.css"

export const Map = () => {
  const position = [48.866667, 2.333333]
  delete L.Icon.Default.prototype._getIconUrl
  L.Icon.Default.mergeOptions({
    iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
    iconUrl: require("leaflet/dist/images/marker-icon.png"),
    shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
    zoomControl: false,
  })

  const [dataPermis, setDataPermis] = useState([])
  const [dataEspacesVerts, setEspacesVerts] = useState([])
  const [open, setOpen] = useState(false)
  const [selectedId, setSelectedId] = useState(null)
  const [filterPermis, setFilterPermis] = useState(true)
  const [filterEspace, setFilterEspace] = useState()

  const onChangeFilter = (value, filter) => {
    switch (value) {
      case "permis":
        setFilterPermis(true)
        break
      case "espace":
        setFilterPermis(false)
        break
      default:
        setFilterPermis(false)
    }
    if (filter) {
      setFilterEspace(filter)
    } else {
      setFilterEspace()
    }
  }

  const openDrawer = (id, event) => {
    setOpen(true)
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return
    }
    setSelectedId(id === selectedId ? null : id)
  }

  const closeDrawer = () => {
    setOpen(false)
    setSelectedId(null)
  }

  const fetchDataEspacesVerts = async () => {
    const result = await fetch(API_URL_ESPACES_VERTS)
    const data = await result.json()
    setEspacesVerts(data.records)
  }

  const fetchDataPermisVegetaliser = async () => {
    const result = await fetch(API_URL_PERMIS_VEGETALISER)
    const data = await result.json()
    setDataPermis(data.records)
  }

  useEffect(() => {
    fetchDataPermisVegetaliser()
    fetchDataEspacesVerts()
  }, [filterPermis])

  const renderPermisDataMarker = () => {
    let data
    if (filterEspace) {
      data = dataPermis.filter(
        (espace) => espace.fields.rv_type === filterEspace
      )
    } else {
      data = dataPermis
    }

    return data.map((espace, keyList) => {
      let customIcon
      switch (espace.fields.rv_type) {
        case "Pieds d’arbres":
          customIcon = L.icon({
            iconUrl: marker1,
            iconSize: [40, 40],
            iconAnchor: [20, 40],
          })
          break
        case "Type 2":
          customIcon = L.icon({
            iconUrl: marker2,
            iconSize: [40, 40],
            iconAnchor: [20, 40],
          })
          break
        default:
          customIcon = L.icon({
            iconUrl: marker2,
            iconSize: [40, 40],
            iconAnchor: [20, 40],
          })
      }

      return (
        <Marker
          key={keyList}
          position={[
            espace.fields.geo_point_2d[0],
            espace.fields.geo_point_2d[1],
          ]}
          icon={customIcon}
          eventHandlers={{
            click: (e) => {
              openDrawer(espace.recordid, e)
            },
          }}
        >
          {open && (
            <PermisVegetaliser
              dataParcel={espace}
              selectedId={selectedId}
              closeDrawer={closeDrawer}
              open={open}
              keyList={keyList}
            />
          )}
        </Marker>
      )
    })
  }

  const renderEspacePolygon = () => {
    return dataEspacesVerts.map((espace, keyList) => {
      if (
        espace.fields.geom &&
        espace.fields.geom.coordinates &&
        espace.fields.geom.coordinates[0]
      ) {
        let positions = espace.fields.geom.coordinates[0].map((item) =>
          item.reverse()
        )
        if (
          espace.fields.geom.type === "Polygon" ||
          espace.fields.geom.type === "MultiPolygon"
        ) {
          return (
            <Polygon
              key={keyList}
              pathOptions={{ color: "red" }}
              positions={positions}
              eventHandlers={{
                click: (e) => {
                  openDrawer(espace.recordid, e)
                },
              }}
            >
              {open && (
                <EspacesVerts
                  keyList={keyList}
                  dataParcel={espace}
                  selectedId={selectedId}
                  closeDrawer={closeDrawer}
                  open={open}
                />
              )}
            </Polygon>
          )
        }
      }
      return <></>
    })
  }

  return (
    <div>
      <Nav />
      <Filter onChangeFilter={onChangeFilter} closeDrawer={closeDrawer} />
      <MapContainer
        center={position}
        zoom={13}
        scrollWheelZoom={false}
        zoomControl={false}
      >
        {filterPermis ? renderPermisDataMarker() : renderEspacePolygon()}
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
      </MapContainer>
    </div>
  )
}
