import axios from "axios";
import { toast } from "react-hot-toast";
import Nav from "../../components/Navbar/Nav";
import BackEndPoints from "../../router/BackEndPoints";
import { useForm } from "react-hook-form";
import "./index.css";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  TextareaAutosize,
  TextField,
} from "@mui/material";

export const Form = () => {
  const { register, handleSubmit } = useForm();

  const sendMail = async (data) => {
    const template = `
  <div>Nom : ${data.lastname}</div>
      <br />
      <div>Prénom : ${data.firstname}</div>
      <br />
      <div>Adresse mail : ${data.email}</div>
      <br />
      <div>Adresse postale : ${data.address}</div>
      <br />
      <div>Décrivez votre projet de végétalisation : ${data.description}</div>
      <br />
      <div>Précisez le lieu de votre projet : ${data.precision}</div>
      <br />
      <div>
        Je souhaite bénéficier de conseils de la régie de Paris Habitat : ${
          data.radio ? "oui" : "non"
        }
      </div>
      <br />
      <div>Image descriptive en pièce jointe.
      )}"/></div>
      <br />
  `;

    try {
      const resp = await axios.post(BackEndPoints.sendMail, {
        template,
      });

      toast.success(resp.data.message);
    } catch (err) {
      toast.error(err.message);
    }
  };

  return (
    <div>
      <Nav />
      <div className="entete">
        <h1>Demande de permis</h1>
      </div>
      <div className="div_form">
        <div className="description">
          <div>Végétalisez votre résidence, c’est permis !</div>
          <br />
          <div>
            Paris Habitat vous propose de participer à la végétalisation de
            votre résidence. Le permis de végétaliser est un nouveau dispositif
            qui permet à chacun de devenir acteur de la végétalisation de vos
            espaces extérieurs. Vous pouvez demander votre permis !
          </div>
        </div>
        <FormControl>
          <TextField
            label="Nom"
            variant="standard"
            {...register("lastname", { required: false })}
            size="small"
            autoFocus
          />
          <TextField
            label="Prénom"
            variant="standard"
            {...register("firstname", { required: false })}
            size="small"
            autoFocus
          />
          <TextField
            label="Adresse mail"
            variant="standard"
            {...register("email", { required: false })}
            size="small"
            autoFocus
          />
          <TextField
            label="Adresse postale"
            variant="standard"
            {...register("address", { required: false })}
            size="small"
            autoFocus
          />
          <FormLabel id="description" className="marginTop">
            Décrivez votre projet de végétalisation
          </FormLabel>
          <TextareaAutosize
            {...register("description", { required: false })}
            className="text-area"
          />

          <FormLabel id="precision" className="marginTop">
            Précisez le lieu de votre projet
          </FormLabel>
          <TextareaAutosize
            {...register("precision", { required: false })}
            className="text-area"
          />
          <FormLabel id="advice" className="marginTop">
            Je souhaite bénéficier de conseils de la régie de Paris Habitat :
          </FormLabel>
          <RadioGroup
            aria-labelledby="advices"
            defaultValue="oui"
            name="advice-radio"
          >
            <FormControlLabel
              value="oui"
              control={<Radio />}
              label="Oui"
              {...register("radio", { required: false })}
            />
            <FormControlLabel
              value="non"
              control={<Radio />}
              label="Non"
              {...register("radio", { required: false })}
            />
          </RadioGroup>
          <button
            className="submit"
            type="button"
            onClick={handleSubmit(sendMail)}
          >
            Envoyer
          </button>
        </FormControl>
      </div>
    </div>
  );
};
