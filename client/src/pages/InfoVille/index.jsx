import Nav from "../../components/Navbar/Nav";
import Villes from "../../Data/data.json";
import "./index.css";
import LaunchIcon from '@mui/icons-material/Launch';

export const InfoVille = () => {
  const renderInfosPdf = (ville) => {
    return Object.keys(ville.infos_pdf).map((label, key) => {
      return (
        <li key={key}>
          <p key={key} className="p_info_ville">
            <a
              href={ville.infos_pdf[label]}
              style={{ textDecoration: "underline" }}
							target="_blank"
							rel="noreferrer"
            >
              {label}
            </a><LaunchIcon className="icon_link"/>
          </p>
        </li>
      )
    })
  }

  const renderComment = (ville) => {
    return ville.how.map((info, key) => {
      return (
        <li key={key}>
          <p key={key} className="p_info_ville">{info}</p>
        </li>
      )
    })
  }

	const demandePermis = (ville) => {
    return ville.demande.map((info, key) => {
      return (
        <li key={key}>
          <p key={key} className="p_info_ville">{info}</p>
        </li>
      )
    })
  }

  return (
    <div className="App">
      <div className="entete">
        <h1>Informations</h1>
      </div>
      <Nav />
      {Villes.map((ville, key) => {
        return (
          <div className="div_informations_ville" key={key}>
            <div className="div_img_info_ville">
              <img
                src={ville.image}
                className="img_info_ville"
                alt={ville.ville}
              />
            </div>
            <div className="div_titre"><h2>Qui ?</h2><img src={require("../../assets/tulipe.png")} alt="Logotype d'une plante" className="icon_png"/></div>
            <p>{ville.who}</p>
						<div className="div_titre"><h2>Où ?</h2><img src={require("../../assets/arrosoir.png")} alt="Logotype d'une plante" className="icon_png"/></div>
            <p>{ville.where}</p>
						<div className="div_titre"><h2>Quoi ?</h2><img src={require("../../assets/etoiles.png")} alt="Logotype d'une plante" className="icon_png"/></div>
            <p>{ville.what}</p>
						<div className="div_titre"><h2>Comment ?</h2><img src={require("../../assets/pot.png")} alt="Logotype d'une plante" className="icon_png"/></div>
            {renderComment(ville)}
						<hr className="hr_info_ville"></hr>
						<div className="div_titre"><h2>Faire sa demande de permis</h2><img src={require("../../assets/arbre_bleu.png")} alt="Logotype d'une plante" className="icon_png"/></div>
						{demandePermis(ville)}
						<div className="div_logo">
							<img src={require("../../assets/logo_plante.png")} alt="Logotype d'une plante"/>
						</div>
						<h2>Liens utiles</h2>
            <ul className="ul_info_ville">
              <li>
                <a href={ville.link_cityhall} target="_blank" rel="noreferrer">
                  Site internet de la ville de Paris
                </a><LaunchIcon className="icon_link"/>
              </li>
              <li>
                <a href={ville.page_license} target="_blank" rel="noreferrer">
                  Tout savoir sur le permis de végétaliser
                </a><LaunchIcon className="icon_link"/>
              </li>
              <li>
                <a href={ville.link_license} target="_blank" rel="noreferrer">
                  Créer son compte
                </a><LaunchIcon className="icon_link"/>
              </li>
              <li>
                <a href={ville.infos_pdf} target="_blank" rel="noreferrer">
                  Informations PDF
                </a><LaunchIcon className="icon_link"/>
              </li>
							<h2>PDF utiles</h2>
              <div className="div_pdf_utiles">
								{renderInfosPdf(ville)}
							</div>
            </ul>
          </div>
        )
      })}
    </div>
  )
}
