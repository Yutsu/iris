import { Button } from "@mui/material"
import { Box } from "@mui/system"
import axios from "axios"
import { useCallback, useEffect, useState } from "react"
import { useLocation, useNavigate } from "react-router-dom"
import Nav from "../../components/Navbar/Nav"
import BackEndPoints from "../../router/BackEndPoints"
import FrontEndPoints from "../../router/FrontEndPoints"

export const Profile = () => {
  const [user, setUser] = useState({})
  const location = useLocation()
  const navigate = useNavigate()
  const allKeys = Object.keys(localStorage)

  const username = location.pathname.split("/")[2]

  const getUser = useCallback(async () => {
    const resp = await axios.post(BackEndPoints.getUser, {
      username: username,
    })
    setUser(resp.data.user)
  }, [username])

  useEffect(() => {
    getUser()
  }, [getUser])

  const signUp = () => {
    navigate(FrontEndPoints.home)
  }

  const favoritePage = () => {
    navigate(FrontEndPoints.favorites)
  }

  const favoris = allKeys.filter((el) => {
    return el.includes("fav_")
  })

  return (
    <div>
      {user ? (
        <div
          style={{
            position: "relative",
            justifyContent: "center",
            textAlign: "center",
            alignItems: "center",
            width: "100%",
          }}
        >
          <Nav />
          <div
            style={{
              backgroundColor: "#6F895D",
              height: "150px",
              position: "relative",
              flexDirection: "column",
              borderBottomLeftRadius: 80,
              borderBottomRightRadius: 80,
              display: "flex",
            }}
          />
          <img
            src="https://fraisesdefrance.fr/wp-content/uploads/2021/02/ronde-big.png"
            width="70px"
            alt="user_profile"
            style={{
              position: "absolute",
              top: 100,
              left: "40%",
            }}
          />
          <div style={{ paddingTop: 48 }}>
            <div>{user.username}</div>
            <Box sx={{ m: 2 }} />
            <div>
              {user.firstname} {user.lastname}
            </div>
            <Box sx={{ m: 2 }} />
            <div>{user.birthdate}</div>
            <Box sx={{ m: 2 }} />
            <div>{user.email}</div>
            <Box sx={{ m: 2 }} />
            <div onClick={favoritePage}>
              {favoris.length} favoris enregistré{favoris.length > 1 ? "s" : ""}
            </div>
          </div>
          <Box sx={{ m: 4 }} />
          <Button
            type="button"
            variant="contained"
            onClick={signUp}
            style={{ backgroundColor: "#1A1B28" }}
          >
            Retour à la carte
          </Button>
        </div>
      ) : (
        <div>User non trouvé</div>
      )}
    </div>
  )
}
