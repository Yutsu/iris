import { Box, Button, FormGroup, Grid, TextField } from "@mui/material"
import axios from "axios"
import Cookies from "js-cookie"
import { useForm } from "react-hook-form"
import { toast } from "react-hot-toast"
import { useNavigate } from "react-router-dom"
import logo from "../../assets/tulipe.png"
import BackEndPoints from "../../router/BackEndPoints"
import FrontEndPoints from "../../router/FrontEndPoints"

export const Login = () => {
  const navigate = useNavigate()

  const { register, handleSubmit } = useForm()

  const connectUser = async (data) => {
    try {
      const resp = await axios.post(BackEndPoints.connectUser, {
        userinfo: data.userinfo,
        password: data.password,
      })

      Cookies.set("accessToken", resp.data.accessToken, {
        expires: 365,
        sameSite: "None",
        secure: true,
      })

      navigate(FrontEndPoints.home)
      toast.success("Vous êtes bien connecté !")
    } catch (err) {
      toast.error("Le pseudo, l'email ou le mot de passe saisi est incorrect")
    }
  }

  const signUp = () => {
    navigate(FrontEndPoints.signup)
  }

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justifyContent="center"
      style={{ position: "relative" }}
    >
      <div
        style={{
          backgroundColor: "#6F895D",
          width: "100%",
          alignItems: "center",
          textAlign: "center",
          height: "170px",
          borderBottomLeftRadius: 80,
          borderBottomRightRadius: 80,
          position: "relative",
        }}
      />
      <h1
        style={{
          position: "absolute",
          top: 60,
          textAlign: "center",
          alignItems: "center",
          justifyContent: "center",
          color: "white",
        }}
      >
        I R I S
      </h1>
      <img
        src={logo}
        alt="logo"
        style={{
          backgroundColor: "#ECE6DF",
          position: "absolute",
          top: 110,
          padding: 30,
          borderRadius: 20,
        }}
      />
      <Box sx={{ m: 5 }} />
      <form onSubmit={handleSubmit(connectUser)}>
        <FormGroup>
          <TextField
            label="Adresse mail / pseudo"
            variant="standard"
            {...register("userinfo", { required: true })}
            size="small"
            autoFocus
          />
          <Box sx={{ m: 2 }} />
          <TextField
            label="Mot de passe"
            variant="standard"
            type="password"
            {...register("password", { required: true })}
            size="small"
          />
          <Box sx={{ m: 5 }} />
          <Button
            type="submit"
            variant="contained"
            style={{ backgroundColor: "#1A1B28" }}
          >
            Se connecter
          </Button>
        </FormGroup>
      </form>
      <Box sx={{ m: 1 }} />
      <Button type="button" variant="outline" onClick={signUp}>
        S'inscrire
      </Button>
    </Grid>
  )
}
