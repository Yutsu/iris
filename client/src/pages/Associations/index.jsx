import Nav from "../../components/Navbar/Nav";
import "./index.css";
import LocationOnIcon from '@mui/icons-material/LocationOn';

export const Associations = () => {



  return (
    <div className="App">
      <div className="entete">
        <h1>Associations</h1>
      </div>

	  <div className="div_asso">
		<div className="div_association_single">
			<h2>Affiche verte manouchian</h2>
			<div className="association_location">
				<LocationOnIcon className="icon_location_association"/>
				<p className="link_asso_location">23 Rue du Groupe Manouchian 75020 Paris</p>
			</div>
			<p>Réunir les riverains et les voisins de la rue du groupe Manouchian, 75020 Paris, afin d’oeuvrer à la végétalisation de la rue et à son animation, à la mise en valeur de la mémoire de l'histoire de la rue et de celle du groupe Manouchian, de veiller à la propreté de la rue et à la préservation de son environnement, de ses aménagements, à la sécurité de ses habitants ainsi qu'à leur protection des nuisances</p>
		</div>
	  </div>

	  <div className="div_asso">
		<div className="div_association_single">
			<h2>Association chlorophylle en folie</h2>
			<div className="association_location">
				<LocationOnIcon className="icon_location_association"/>
				<p className="link_asso_location">79 Rue des Entrepreneurs 75015 Paris</p>
			</div>
			<p>Créer un groupe de personnes, aimant ou aspirant à développer dans Paris la végétalisation de ses rues et balcons : de ce fait augmenter une relation sociale de proximité par des rencontres amicales, culturelles, artistiques, scientifiques.</p>
		</div>
	  </div>

	  <div className="div_asso">
		<div className="div_association_single">
			<h2>Ampere vegetale</h2>
			<div className="association_location">
				<LocationOnIcon className="icon_location_association"/>
				<p className="link_asso_location">42 Rue Ampère 75017 Paris</p>
			</div>
			<p>Favoriser la végétalisation de la rue ampère et du quartier Péreire Wagram pour améliorer le cadre de vie des habitants du 17ème arrondissement de Paris. Plus largement, sensibiliser l'opinion et les décideurs publics au développement des espaces verts et accompagner toutes les activités qui promeuvent un mode de vie respectueux de l'environnement et éco-responsable.</p>
		</div>
	  </div>

	  <div className="div_asso">
		<div className="div_association_single">
			<h2>Association city bio</h2>
			<div className="association_location">
				<LocationOnIcon className="icon_location_association"/>
				<p className="link_asso_location">7 Avenue Léon Bollée 75013 Paris</p>
			</div>
			<p>Participer au rayonnement écologique de la ville de Paris : promouvoir le recyclage des déchets, sa végétalisation, le développement d'une agriculture urbaine sur les toits. De plus, donner une éducation sur une vie citadine éco-responsable.</p>
		</div>
	  </div>


	  <div className="div_asso">
		<div className="div_association_single">
			<h2>Chapelle 4-18</h2>
			<div className="association_location">
				<LocationOnIcon className="icon_location_association"/>
				<p className="link_asso_location">4 Place de la Chapelle 75018 Paris</p>
			</div>
			<p>Sécuriser les accès à l'immeuble, aménager et animer les parties communes dont l'ex-loge de concierge, participer à l'aménagement et à la végétalisation de la cour intérieure.</p>
		</div>
	  </div>


	  <div className="div_asso">
		<div className="div_association_single">
			<h2>Pépins production</h2>
			<div className="association_location">
				<LocationOnIcon className="icon_location_association"/>
				<p className="link_asso_location">40 Rue du Télégraphe, 75020 Paris</p>
			</div>
			<p>Créée il y a maintenant 5 ans, Pépins production est une association soutenue par le programme “J’agis pour la nature” de la Fondation Nicolas Hulot qui se donne pour mission de permettre à chaque citoyen.ne, quel que soit son âge, son origine ou son parcours social, de vivre dans un environnement sain et un climat social stimulant, au contact du végétal. Grâce à l’appel à projet Parisculteurs, l’association a pu obtenir deux terrains : Flore Urbaine, dans le 19e, et la Pépinière Chanzy, dans le 11e arrondissement de Paris. Ces derniers leur permettent de planter une grande variété de plantes en plein Paris et de proposer ainsi aux habitants des végétaux en circuit court.</p>
		</div>
	  </div>

	  <div className="div_asso">
		<div className="div_association_single">
			<h2>Aux arbres citoyens</h2>
			<p>Nous organisons des cueillettes chez les habitants qui nous en font la demande en mobilisant nos bénévoles. A la fin de la cueillette, les fruits sont partagés entre les cueilleurs bénévoles, les propriétaires et des associations locales d’aide alimentaire. On cueille les fruits, et on les partage ! Et en septembre, toute une équipe de passionnés qui ont décidé de végétaliser Paris en verdissant la jolie rue de Bazeilles.</p>
		</div>
	  </div>

	  <div className="div_asso">
		<div className="div_association_single">
			<h2>Des tours au jardin</h2>
			<div className="association_location">
				<LocationOnIcon className="icon_location_association"/>
				<p className="link_asso_location">11 Rue Caillaux 75013 Paris</p>
			</div>
			<p>Cette association a pour objet de cultiver, organiser, animer et gérer le "jardin partagé" mis à sa disposition dans l'esprit de la Charte "Main Verte", en particulier en respectant les principes d'une gestion écologique, et en assurer l'accès régulier au public. Organiser des activités pédagogiques, intergénérationnelles, des événements artistiques et culturels, des animations de quartier, en lien le développement durable et promouvoir toutes les actions favorisant la végétalisation et l'agriculture urbaine dans un objectif de développement durable. L’objectif est de se former, apprendre, acquérir des savoir-faire, recevoir des conseils, mais aussi transmettre aux autres : développer les partenariats avec les associations, les organismes et les écoles du 13e arrondissement de Paris et au-delà dans les domaines concernant le développement durable. De plus, inciter à la création et au développement de "jardins partagés" ou d'espaces végétalisés, toute autre activité ayant trait à la nature et au développement durable.</p>
		</div>
	  </div>

	  <div className="div_asso">
		<div className="div_association_single">
			<h2>L'association du collectif de la rue de Montreuil de Paris 11eme</h2>
			<div className="association_location">
				<LocationOnIcon className="icon_location_association"/>
				<p className="link_asso_location">33 Rue de Montreuil 75011 Paris</p>
			</div>
			<p>Représenter et valoriser les initiatives, les savoir-faire des commerçants, artisans et habitants de la Rue de Montreuil. Valoriser et renforcer l'identité de la Rue de Montreuil et développer des projets de végétalisation et de mobilier urbain pour permettre à tout le monde de s'approprier la rue de manière solidaire tout en cherchant à limiter les ilots de chaleur. Le but est de créer une dynamique collective entre les commerçants et les habitants pour animer et faire vivre la rue ensemble (micro-évènements, projets ouverts, etc) et d’animer et créer un lieu d'échanges et de dialogues entre les habitants, les entreprises et les employés pour partager des connaissances, des histoires, et renforcer les solidarités. Cela permet une activité économique à l'association pour trouver les ressources financières nécessaires à son fonctionnement.</p>
		</div>
	  </div>


      <Nav />
    </div>
  )
}
