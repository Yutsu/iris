import DeleteIcon from "@mui/icons-material/Delete"
import FenceRoundedIcon from "@mui/icons-material/FenceRounded"
import GrassIcon from "@mui/icons-material/Grass"
import LocalFloristIcon from "@mui/icons-material/LocalFlorist"
import NatureRoundedIcon from "@mui/icons-material/NatureRounded"
import NotInterestedRoundedIcon from "@mui/icons-material/NotInterestedRounded"
import PanoramaFishEyeRoundedIcon from "@mui/icons-material/PanoramaFishEyeRounded"
import SignpostRoundedIcon from "@mui/icons-material/SignpostRounded"
import YardRoundedIcon from "@mui/icons-material/YardRounded"
import { Box } from "@mui/system"
import { useEffect, useState } from "react"
import ReactLoading from "react-loading"
import Nav from "../../components/Navbar/Nav"
import {
  API_URL_ESPACES_VERTS,
  API_URL_PERMIS_VEGETALISER,
} from "../../constants/api"
import "./index.css"

export const Favorites = () => {
  const [data, setData] = useState([])
  const allKeys = Object.keys(localStorage)
  const AllFavorites = []
  const [loading, setLoading] = useState(false)

  // const [test, setTest] = useState(allKeys)

  const keys = allKeys.filter((el) => {
    return el.includes("fav_")
  })

  keys.forEach((el) => AllFavorites.push({ id: el, statut: localStorage[el] }))

  const favorites = AllFavorites.filter((el) => {
    return el.statut === "true"
  })

  const fetchData = async () => {
    const resultPermis = await fetch(API_URL_PERMIS_VEGETALISER)
    const dataPermis = await resultPermis.json()

    const resultEspace = await fetch(API_URL_ESPACES_VERTS)
    const dataEspaces = await resultEspace.json()

    const datas = dataPermis["records"].concat(dataEspaces.records)
    setData(datas)
  }

  const deleteFav = (id) => {
    // setTest(localStorage.removeItem(id))
    localStorage.removeItem(id)
  }

  useEffect(() => {
    fetchData().then((result) => {
      if (!result) {
        setLoading(true)
      } else {
        setLoading(false)
      }
    })
    // }, [test])
  }, [])

  const Fav = () => {
    return favorites.map((x) => {
      const object = data.find((y) => y.recordid === x.id.split("fav_")[1])

      const renderPerimeter = () => {
        if (object.fields.perimeter) {
          return `Périmètre : ${object.fields.perimeter.toFixed(2)}m²`
        }
        return ""
      }
      const renderIconDecoMail = () => {
        if (
          object.fields.categorie === "Decoration" ||
          object.fields.categorie === "Mail"
        ) {
          return <LocalFloristIcon />
        }
      }

      const renderIconJardin = () => {
        if (
          object.fields.categorie === "Jardiniere" ||
          object.fields.categorie === "Jardin" ||
          object.fields.categorie === "Jardinet"
        ) {
          return <YardRoundedIcon />
        }
      }

      const renderIconPelouse = () => {
        if (
          object.fields.categorie === "Pelouse" ||
          object.fields.categorie === "Plate-bande" ||
          object.fields.categorie === "Talus" ||
          object.fields.categorie === "Terre-plein" ||
          object.fields.categorie === "Square"
        ) {
          return <GrassIcon />
        }
      }

      const renderFieldType = () => {
        if (object?.fields) {
          switch (object?.fields?.rv_type) {
            case "Pieds d’arbres":
              return <NatureRoundedIcon />
            case "Jardinières mobiles ou keyholes":
              return <YardRoundedIcon />
            case "Fosses":
              return <PanoramaFishEyeRoundedIcon />
            case "Murs ou clôtures":
              return <FenceRoundedIcon />
            case "Potelets (poteaux anti-stationnement)":
              return <SignpostRoundedIcon />
            case "Barrières fixes anti-stationnement":
              return <NotInterestedRoundedIcon />
            default:
              break
          }
        }
      }

      return (
        <div
          key={x.id}
          style={{
            padding: 10,
            border: "2px solid #bbbeed",
            borderRadius: 8,
            marginBottom: 24,
          }}
        >
          {object && object.fields && (
            <>
              <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div>
                  {object.fields.rv_l_deb && (
                    <p style={{ fontWeight: "bold" }}>
                      {`${object?.fields?.rv_l_deb.toLowerCase()}, ${
                        object?.fields?.rv_arrdt
                      }`}
                    </p>
                  )}
                  {object.fields.nom_ev && (
                    <p style={{ fontWeight: "bold" }}>
                      {object.fields.adresse_numero}{" "}
                      {object.fields.adresse_typevoie.toLowerCase()}{" "}
                      {object.fields.adresse_libellevoie.toLowerCase()},{" "}
                      {object.fields.adresse_codepostal}
                    </p>
                  )}
                </div>
                <div
                  onClick={() => deleteFav(x.id)}
                  style={{ textAlign: "right" }}
                >
                  <DeleteIcon style={{ color: "#8b8c93", paddingLeft: 4 }} />
                </div>
              </div>
              {object.fields.categorie && (
                <p className="type">
                  {renderIconDecoMail()}
                  {renderIconJardin()}
                  {renderIconPelouse()}
                  {object.fields.categorie}
                </p>
              )}
              <div
                style={{
                  margin: "10px 0 10px",
                  display: "flex",
                  alignItems: "center",
                  color: "#6F895D",
                }}
              >
                {renderFieldType()}
                {object.fields.rv_type && (
                  <p style={{ paddingLeft: 8 }}>{object?.fields.rv_type}</p>
                )}
              </div>
              <Box sx={{ m: 1 }} />
              {object.fields.statut === "13 - Permis délivré" && (
                <p className="statusFav delivred">
                  <span></span>Permis délivré
                </p>
              )}
              {object.fields.surface_totale_reelle && (
                <p>
                  Surface totale réelle : {object.fields.surface_totale_reelle}
                  m²
                </p>
              )}
              {object.fields.presence_cloture && (
                <p>Présence de culture : {object.fields.presence_cloture}</p>
              )}
              {object.fields.perimeter && <p>{renderPerimeter()}</p>}
            </>
          )}
        </div>
      )
    })
  }

  return (
    <div>
      <Nav />
      <h1 style={{ textAlign: "center", paddingTop: 36 }}>Favoris</h1>
      <div style={{ padding: 24 }}>
        {!loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              paddingTop: 80,
            }}
          >
            <ReactLoading
              type="spin"
              color="#6f895d"
              width={120}
              height={270}
            />
          </div>
        ) : (
          <Fav />
        )}
      </div>
    </div>
  )
}
