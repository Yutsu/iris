import { yupResolver } from "@hookform/resolvers/yup";
import { AccountCircle } from "@mui/icons-material";
import BadgeIcon from "@mui/icons-material/Badge";
import EmailIcon from "@mui/icons-material/Email";
import EmojiPeopleIcon from "@mui/icons-material/EmojiPeople";
import KeyIcon from "@mui/icons-material/Key";
import PasswordIcon from "@mui/icons-material/Password";
import {
  Box,
  Button,
  FormGroup,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  TextField,
} from "@mui/material";
import { LocalizationProvider, MobileDatePicker } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import axios from "axios";
import dayjs from "dayjs";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-hot-toast";
import { useNavigate } from "react-router-dom";
import * as Yup from "yup";
import BackEndPoints from "../../router/BackEndPoints";
import FrontEndPoints from "../../router/FrontEndPoints";

export const Signup = () => {
  const navigate = useNavigate();

  const formSchema = Yup.object().shape({
    password: Yup.string()
      .required("Le mot de passe est obligatoire")
      .min(3, "Le mot de passe doit faire minimum 3 caractères"),
    confirmpassword: Yup.string()
      .required("Le mot de passe est obligatoire")
      .oneOf([Yup.ref("password")], "Le mot de passe ne correspond pas"),
  });

  const formOptions = { resolver: yupResolver(formSchema) };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(formOptions);

  const [birthdate, setBirthDate] = useState(dayjs(Date.now()));

  const handleDate = (newDate) => {
    setBirthDate(newDate);
  };

  const login = () => {
    navigate(FrontEndPoints.login);
  };

  const createUser = async (data) => {
    try {
      await axios.post(BackEndPoints.addUser, {
        firstname: data.firstname,
        lastname: data.lastname,
        birthdate: birthdate,
        username: data.username,
        email: data.email,
        password: data.password,
      });

      navigate(FrontEndPoints.login);
    } catch (err) {
      toast.error(err.response.data.message);
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <h1 style={{ textAlign: "center", paddingTop: 32, color: "#6F895D" }}>
        Création de compte
      </h1>
      <form onSubmit={handleSubmit(createUser)} style={{ padding: "3rem" }}>
        <FormGroup>
          <InputLabel htmlFor="input-with-icon-adornment">Prénom</InputLabel>
          <OutlinedInput
            {...register("firstname", { required: true })}
            size="small"
            autoFocus
            startAdornment={
              <InputAdornment position="start">
                <BadgeIcon />
              </InputAdornment>
            }
          />
          {errors.firstname && (
            <div className="invalid-feedback">
              <p style={{ color: "#e85451" }}>Le prénom est requis</p>
            </div>
          )}
          <Box sx={{ m: 2 }} />
          <InputLabel htmlFor="input-with-icon-adornment">
            Nom de famille
          </InputLabel>
          <OutlinedInput
            {...register("lastname", { required: true })}
            size="small"
            startAdornment={
              <InputAdornment position="start">
                <AccountCircle />
              </InputAdornment>
            }
          />
          {errors.lastname && (
            <div className="invalid-feedback">
              <p style={{ color: "#e85451" }}>Le nom de famille est requis</p>
            </div>
          )}
          <Box sx={{ m: 3 }} />
          <MobileDatePicker
            label="Date de naissance"
            inputFormat="DD/MM/YYYY"
            value={birthdate}
            onChange={handleDate}
            renderInput={(params) => <TextField {...params} />}
          />
          <Box sx={{ m: 2 }} />
          <InputLabel htmlFor="input-with-icon-adornment">Pseudo</InputLabel>
          <OutlinedInput
            {...register("username", { required: true })}
            size="small"
            startAdornment={
              <InputAdornment position="start">
                <EmojiPeopleIcon />
              </InputAdornment>
            }
          />
          {errors.username && (
            <div className="invalid-feedback">
              <p style={{ color: "#e85451" }}>Le pseudo est requis</p>
            </div>
          )}
          <Box sx={{ m: 2 }} />
          <InputLabel htmlFor="input-with-icon-adornment">
            Adresse mail
          </InputLabel>
          <OutlinedInput
            {...register("email", { required: true })}
            size="small"
            startAdornment={
              <InputAdornment position="start">
                <EmailIcon />
              </InputAdornment>
            }
          />
          {errors.email && (
            <div className="invalid-feedback">
              <p style={{ color: "#e85451" }}>L'adresse mail est requise</p>
            </div>
          )}
          <Box sx={{ m: 2 }} />
          <InputLabel htmlFor="input-with-icon-adornment">
            Mot de passe
          </InputLabel>
          <OutlinedInput
            {...register("password", { required: true })}
            size="small"
            type="password"
            startAdornment={
              <InputAdornment position="start">
                <PasswordIcon />
              </InputAdornment>
            }
          />
          {errors.password && (
            <div className="invalid-feedback">
              <p style={{ color: "#e85451" }}>Le mot de passe est requis</p>
            </div>
          )}
          <Box sx={{ m: 2 }} />
          <InputLabel htmlFor="input-with-icon-adornment">
            Confirmation du mot de passe
          </InputLabel>
          <OutlinedInput
            {...register("confirmpassword", { required: true })}
            size="small"
            type="password"
            startAdornment={
              <InputAdornment position="start">
                <KeyIcon />
              </InputAdornment>
            }
          />
          {errors.confirmpassword && (
            <div className="invalid-feedback">
              <p style={{ color: "#e85451" }}>
                Le mot de passe entré est incorrect avec le premier mot de passe
              </p>
            </div>
          )}
          <Box sx={{ m: 2 }} />
          <Button type="submit" variant="contained">
            Valider
          </Button>
          <Box sx={{ m: 1 }} />
          <Button type="button" variant="outline" onClick={login}>
            Retour
          </Button>
        </FormGroup>
      </form>
    </LocalizationProvider>
  );
};
