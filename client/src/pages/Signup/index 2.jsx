import { useState } from "react";
import { useForm } from "react-hook-form";
import axios from "axios";
import BackEndPoints from "../../router/BackEndPoints";
import { useNavigate } from "react-router-dom";
import FrontEndPoints from "../../router/FrontEndPoints";
import { toast } from "react-hot-toast";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Box, Button, FormGroup, TextField } from "@mui/material";
import { LocalizationProvider, MobileDatePicker } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";

export const Signup = () => {
  const navigate = useNavigate();

  const formSchema = Yup.object().shape({
    password: Yup.string()
      .required("Password is mendatory")
      .min(3, "Password must be at 3 char long"),
    confirmpassword: Yup.string()
      .required("Password is mendatory")
      .oneOf([Yup.ref("password")], "Passwords does not match"),
  });

  const formOptions = { resolver: yupResolver(formSchema) };

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm(formOptions);

  const [birthdate, setBirthDate] = useState(dayjs(Date.now()));

  const handleDate = (newDate) => {
    setBirthDate(newDate);
  };

  const createUser = async (data) => {
    try {
      await axios.post(BackEndPoints.addUser, {
        firstname: data.firstname,
        lastname: data.lastname,
        birthdate: birthdate,
        username: data.username,
        email: data.email,
        password: data.password,
      });

      navigate(FrontEndPoints.login);
    } catch (err) {
      toast.error(err.response.data.message);
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <form onSubmit={handleSubmit(createUser)}>
        <FormGroup>
          <TextField
            placeholder="firstname"
            {...register("firstname", { required: true })}
          />
          <Box sx={{ m: 2 }} />
          {errors.firstname && <p>First name is required.</p>}
          <TextField
            placeholder="lastname"
            {...register("lastname", { required: true })}
          />
          <Box sx={{ m: 2 }} />
          <MobileDatePicker
            label="Date de naissance"
            inputFormat="DD/MM/YYYY"
            value={birthdate}
            onChange={handleDate}
            renderInput={(params) => <TextField {...params} />}
          />
          <Box sx={{ m: 2 }} />
          <TextField
            placeholder="username"
            {...register("username", { required: true })}
          />
          <Box sx={{ m: 2 }} />
          <TextField
            placeholder="email"
            {...register("email", { required: true })}
          />
          <Box sx={{ m: 2 }} />
          <TextField
            placeholder="password"
            type={"password"}
            {...register("password", { required: true })}
          />
          <Box sx={{ m: 2 }} />
          <div className="invalid-feedback">{errors.password?.message}</div>
          <TextField
            placeholder="confirmpassword"
            type={"password"}
            {...register("confirmpassword", { required: true })}
          />
          <Box sx={{ m: 2 }} />
          <div className="invalid-feedback">
            {errors.confirmpassword?.message}
          </div>
          <Button type="submit" variant="contained">
            S'inscrire
          </Button>
        </FormGroup>
      </form>
    </LocalizationProvider>
  );
};
