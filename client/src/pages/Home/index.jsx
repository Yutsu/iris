import { Map } from "../../components/Map"
import Nav from "../../components/Navbar/Nav"
import "./index.css"

export const Home = () => {
  return (
    <div className="index">
      <Nav />
      <Map />
    </div>
  )
}
