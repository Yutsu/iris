import axios from "axios"
import { Toaster } from "react-hot-toast"
import "./App.css"
import { Router } from "./router"
axios.defaults.baseURL = process.env.REACT_APP_URL

const App = () => {
  return (
    <>
      <Router />
      <Toaster />
    </>
  )
}

export default App
