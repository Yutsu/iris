# LANCER LE PROJET

**Lancer docker :**
Allumer docker en cliquant sur l'application Docker Desltop
Pui faire la commande suivante dans le projet IRIS

### `docker-compose up -d`

**Lancer le serveur dans iris/server :**

### `npm run start`

**Lancer le client dans iris/client :**

### `npm run start`

# COMMANDE GIT

**Pusher sur gitlab :**
git add .
git commit -m "Le message du commit"
git push

**Etre à jour sur la branche main et aussi sur votre branche de développement et le pusher :**
git checkout main
git pull
git checkout votreBranche
git pull origin main
Le terminal vous indiquera un message de merge request, tapez :wq pour sortir du message
git push

**Quand il y a un conflit quand vous avez pull main :**
Aller sur le menu du vscode à gauche et cliquez sur la troisième icône "Source Control"
Regarder la première section, ce sont les fichiers qui ont été modifié
Faites vos changements
Ensuite refaire les commandes git add . commit push comme citez au dessus

**Voir les fichiez que vous avez modifié :**
Soit vous le voyez sur la troisième icône du vsocde dans la parie "Changes"
Soit sur le terminal, mettez git status

**Voir tout le code modifié sur votre branche de développement :**
git diff

**Voir tous les commits que vous avez fait :**
git log

**Si jamais le projet ne se lance pas**
C'est que vous avez peut être pas mis à jour le projet, c'est à dire que d'autre lib on été rajouté du coup
faire un npm install ça mettra les packages à jours
Ou vous avez mal lancé un truc, pinguez sur slack lol
